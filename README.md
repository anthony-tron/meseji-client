# meseji-client

This project is a web client for the meseji-server.It has been initialized with `npx create-react-app meseji-client --template typescript`.

Current URL : <http://anthonytron.alwaysdata.net>

## Features

As of 2021/11/04, more importantly, you can:
- create an account, log in, disconnect
- send messages in a room and see others' reply
- be notified of a new message when you are scrolling old messages

Behind the scene, this app...
- fetch only new messages using long polling
- handles abrupt server disconnection

## Development conventions

### Importing modules

Modules are imported in the following order:
1. **External dependencies**: react, react-router-dom, axios, react-bootstrap, [meseji-types](<https://www.npmjs.com/package/meseji-types>)
2. **utils modules**: context, hooks
3. own project modules

### Making API calls

To prevent bugs, this project should respect the following rules:

1. **GET requests** should use the following signature of `axios.get`: `get<ResponseBody>(...)` because the parameters of a GET request is set directly in the url where `ResponseBody` is an interface that represents the data that should be present in the response.
   
2. **POST requests** should use the following signature of `axios.post`: `post<RequestBody, AxiosResponse<ResponseBody>>` where `RequestBody` is an interface that represents the data that will be sent, and `ResponseBody` is an interface that represents the data that should be present in the response.

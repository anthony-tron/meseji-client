import {createContext} from "react";
import {UserModel} from "meseji-types";

interface LogInContextProperties {
    firstAuthed: boolean,
    logged: boolean,
    user: UserModel | undefined
}

export const LogInContext = createContext<LogInContextProperties>({
    firstAuthed: false,
    logged: false,
    user: undefined,
});

import {FormEvent, RefObject, useEffect, useState} from "react";

export function useFormInput(initialValue: any) {
    const [value, setValue] = useState(initialValue);

    function handleChange(event: any) {
        setValue(event.target.value);
    }

    return {
        value: value,
        onChange: handleChange
    };
}

export function useFormSubmit(callback: () => void) {
    return (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        callback();
    }
}

export function useInterval(delay: number, callback: () => void) {
    useEffect(() => {
        let id = setInterval(callback, delay);

        return () => {
            clearInterval(id);
        };
    }, [delay, callback]);
}

export function useIntervalImmediately(delay: number, callback: () => void) {
    useEffect(() => {
        let id = setInterval((function repeatedFunction() {
            callback();
            return repeatedFunction; // actually subscribes to setInterval
        })() /* first executed here */, delay);

        return () => {
            clearInterval(id);
        };
    }, [delay, callback]);
}

export function useScroll(ref: RefObject<HTMLElement>) {

    const [state, setState] = useState({
        x: 0,
        y: 0,
        atTop: false,
        atBottom: false,
        atLeft: false,
        atRight: false,
    });

    useEffect(() => {

        const currentRef = ref.current;

        function handle() {
            if (currentRef === null)
                return;

            const {scrollLeft, scrollTop, scrollWidth, scrollHeight, clientWidth, clientHeight} = currentRef;

            setState({
                x: scrollLeft,
                y: scrollTop,
                atLeft: scrollLeft <= 0,
                atRight: scrollWidth - scrollLeft === clientWidth,
                atTop: scrollTop <= 0,
                atBottom: scrollHeight - scrollTop === clientHeight,
            });
        }

        if (currentRef !== null)
            currentRef.addEventListener('scroll', handle);

        return () => {
            // unsubscribe
            currentRef?.removeEventListener('scroll', handle);
        };
    }, [ref]);

    return state;
}

import React, {useEffect, useState} from 'react';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import axios from "axios";
import {Alert, Container, Toast} from "react-bootstrap";
import {PingResponse, UserModel, FetchUserResponse} from "meseji-types";
import {LogInContext} from "./utils/contexts";
import {useIntervalImmediately} from "./utils/hooks";
import LogIn from "./endpoints/LogIn";
import Register from "./endpoints/Register";
import Home from "./endpoints/Home";
import TopNav from "./components/nav/TopNav";
import RoomRouter from "./components/RoomRouter";
import RequiredAuth from "./components/RequiredAuth";

import 'bootstrap/dist/css/bootstrap.min.css';

export default function App() {

    const [firstAuthed, setFirstAuthed] = useState(false);
    const [user, setUser] = useState<UserModel|undefined>(undefined);
    const [logged, setIsLogged] = useState(false);
    const [sessionAborted, setSessionAborted] = useState(false);

    useEffect(() => {
        if (logged)
            axios.get<FetchUserResponse>('/api/user/by/current')
                .then(response => {
                    if (response.data.success) {
                        setUser(response.data.result);
                    } else {
                        alert('Désolé, impossible de trouver vos données utilisateur');
                    }
                }).catch(reason => {
                    console.error(reason);
                    alert('Attention : Un problème technique est survenu.');
                });
    }, [logged]);

    useIntervalImmediately(1000, () => {
        axios.get<PingResponse>('/api/ping')
            .then(
                response => {
                    setFirstAuthed(true);
                    setIsLogged(wasLogged => {
                        if (wasLogged && !response.data.success)
                            setSessionAborted(true);
                        return response.data.success;
                    })
                }
            )
            .catch(() => setSessionAborted(true));
    });

    return (
        <LogInContext.Provider value={{logged, user, firstAuthed}}>
            <BrowserRouter>
                <TopNav />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/login" component={LogIn} />
                    <Route path="/register" component={Register} />
                    <Route path={'/room/:code?'}>
                        <Container className="mt-3">
                            <RequiredAuth>
                                <RoomRouter />
                            </RequiredAuth>
                        </Container>
                    </Route>
                    <Route>
                        <Container className="mt-3">
                            <Alert variant="danger">
                                Oups ! Cette page n'existe pas. <Link to="/">Revenir à l'accueil</Link>
                            </Alert>
                        </Container>
                    </Route>
                </Switch>
            </BrowserRouter>

            <div style={{
                position: 'absolute',
                top: 4,
                right: 4,
                cursor: 'pointer'}}>
                <Toast show={sessionAborted}
                       onClick={() => document.location.href = '/login'}>
                    <Toast.Header className="text-danger">
                        Votre session a expiré
                    </Toast.Header>
                    <Toast.Body>
                        Ceci est peut-être du à un redémarrage du serveur. Veuillez vous reconnecter.
                    </Toast.Body>
                </Toast>
            </div>
        </LogInContext.Provider>
    );
}

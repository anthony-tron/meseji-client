import {Container} from "react-bootstrap";
import LogInForm from "../components/forms/LogInForm";

export default function LogIn() {
    return (
        <Container>
            <h1>Connectez-vous</h1>
            <LogInForm />
        </Container>
    );
}
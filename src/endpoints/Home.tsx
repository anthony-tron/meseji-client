import {useContext} from "react";
import {Container} from "react-bootstrap";
import {LogInContext} from "../utils/contexts";

export default function Home() {

    const logInContext = useContext(LogInContext);

    return (
        <Container>
            <p>Bienvenue {logInContext.logged} !</p>
        </Container>
    );
}
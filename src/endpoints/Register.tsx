import {Container} from "react-bootstrap";
import RegisterForm from "../components/forms/RegisterForm";

export default function Register() {
    return (
        <Container>
            <h1>Inscrivez-vous</h1>
            <RegisterForm />
        </Container>
    );
}
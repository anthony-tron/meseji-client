import {Nav, NavDropdown} from "react-bootstrap";
import {Link, useLocation} from "react-router-dom";
import React, {useContext} from "react";
import {LogInContext} from "../../utils/contexts";
import axios from "axios";
import {LogOutResponse} from "meseji-types";

interface Props {
    variant?: 'pills' | 'tabs',
    className?: string,
    linkClassName?: string,
}

interface EndPoint {
    title: string,
    path: string,
    condition?: () => boolean,
}

export default function Navigation(props: Props) {

    const location = useLocation();
    const {logged, user} = useContext(LogInContext);

    const endPoints : EndPoint[] = [
        {
            title: 'Accueil',
            path: '/',
        },
        {
            title: 'Chat',
            path: '/room',
            condition: () => logged,
        },
        {
            title: 'Connexion',
            path: '/login',
            condition: () => !logged,
        },
        {
            title: 'Pas de compte ?',
            path: '/register',
            condition: () => !logged
        },
    ];

    function logOut() {
        axios.get<LogOutResponse>('/api/logout')
            .then(
                response => {
                    if (response.data.success)
                        window.location.href = '/';
                    else
                        alert('Impossible de se déconnecter');
                }
            )
            .catch(reason => {
                alert('Impossible de se déconnecter. Veuillez réessayer dans quelques instants.');
                console.error(reason);
            });
    }

    return (
        <Nav variant={props.variant}
             activeKey={location.pathname}
             className={props.className}
        >
            {
                endPoints
                    .filter(e => e.condition === undefined || e.condition())
                    .map((e) =>
                        <Nav.Link as={Link} to={e.path} key={e.path} eventKey={e.path} className={props.linkClassName}>
                            {e.title}
                        </Nav.Link>
                    )
            }

            { logged &&
                <NavDropdown style={{zIndex: 2000}} id="user-menu" title={user?.name || 'user'}>
                    <NavDropdown.Item onClick={logOut}>
                        Déconnexion
                    </NavDropdown.Item>
                </NavDropdown>
            }
        </Nav>
    );
}

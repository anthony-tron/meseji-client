import {Link} from "react-router-dom";
import Navigation from "./Navigation";
import {Navbar, Container, Nav} from "react-bootstrap";

export default function TopNav() {

    return (
        <Navbar collapseOnSelect bg="dark" variant="dark" expand="lg">
            <Container>
                <Navbar.Brand>
                    <Nav.Link as={Link} to="/">meseji</Nav.Link>
                </Navbar.Brand>

                <Navbar.Toggle aria-controls="responsive-navbar"/>
                <Navbar.Collapse id="responsive-navbar">
                    <Navigation />
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );

}
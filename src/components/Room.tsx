import {RoomModel} from "meseji-types";
import RoomMessageFeed from "./RoomMessageFeed";
import PostRoomMessageForm from "./forms/PostRoomMessageForm";

interface Props {
    roomModel: RoomModel,
}

export default function Room(props: Props) {

    return (
        <>
            <h1>{props.roomModel.name}</h1>
            <RoomMessageFeed height={320} roomId={props.roomModel.id} />
            <PostRoomMessageForm roomId={props.roomModel.id} />
        </>
    );
}

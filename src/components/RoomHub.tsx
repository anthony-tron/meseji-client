import {Col, Row} from "react-bootstrap";
import { RoomModel } from "meseji-types";
import Room from "./Room";
import RoomBrowser from "./RoomBrowser";

interface Props {
    roomModel?: RoomModel,
}

export default function RoomHub(props: Props) {
    return (
        <Row lg={2} sm={1} xs={1}>
            <Col lg={4}>
                <RoomBrowser />
            </Col>
            <Col lg={8}>
            { props.roomModel === undefined
                ? <p>Cliquez pour rejoindre une salle</p>
                : <Room roomModel={props.roomModel} />
            }
            </Col>
        </Row>
    );
}

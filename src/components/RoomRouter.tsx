import {useContext, useEffect, useState} from "react";
import {useParams} from "react-router";
import axios from "axios";
import { FetchRoomByCodeResponse, RoomModel } from "meseji-types";
import {LogInContext} from "../utils/contexts";
import RoomHub from "./RoomHub";

export default function RoomRouter() {
    const {code} = useParams<{ code?: string }>();
    const [room, setRoom] = useState<RoomModel | undefined>(undefined);
    const {logged} = useContext(LogInContext);

    useEffect(() => {
        if (!logged || code === undefined) return;

        axios.get<FetchRoomByCodeResponse>(`/api/room/code/${code}`)
            .then(result => {
                if (result.data.success)
                    setRoom(result.data.result);
                else
                    alert("On dirait que cette salle n'existe pas. Désolé !");
            })
            .catch(reason => {
                console.error(reason);
                alert('Attention : une erreur est survenue.');
            });
    }, [code, logged]);

    return <RoomHub roomModel={room} />;
}
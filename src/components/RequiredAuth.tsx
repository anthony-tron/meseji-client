import {ReactNode, useContext} from "react";
import {Link} from "react-router-dom";
import {Alert, Spinner} from "react-bootstrap";
import {LogInContext} from "../utils/contexts";

interface Props {
    children: ReactNode,
}

export default function RequiredAuth(props: Props) {
    const {logged, firstAuthed} = useContext(LogInContext);

    if (!firstAuthed) {
        return (
            <Spinner animation="border" variant="primary" />
        );
    } else if (logged) {
        // already authed and logged
        return <>{props.children}</>
    } else {
        // auth failed, so it's forbidden
        return (
            <Alert variant="danger">
                Vous n'avez pas accès à ce contenu. Veuillez vous <Link to="/login">connecter</Link>.
            </Alert>
        );
    }
}
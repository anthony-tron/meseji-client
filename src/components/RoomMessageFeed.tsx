import {createRef, useContext, useEffect, useState} from "react";
import axios from "axios";
import {Button, Col, Row, Spinner} from "react-bootstrap";
import {RoomMessageModel, FetchRoomMessagesResponse} from "meseji-types";
import {useInterval, useScroll} from "../utils/hooks";
import RoomMessage from "./RoomMessage";
import {LogInContext} from "../utils/contexts";


interface Props {
    roomId: number,
    height: number, // in px
}

export default function RoomMessageFeed(props: Props) {

    const {logged, user} = useContext(LogInContext);

    const root = createRef<HTMLDivElement>();
    const {atBottom} = useScroll(root);
    const [feed, setFeed] = useState<RoomMessageModel[]>([]);
    const [newMessagesCount, setNewMessagesCount] = useState(0);
    const [needsScrollToBottom, setNeedsScrollToBottom] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [noMoreContent, setNoMoreContent] = useState(false);

    const now = () => (new Date()).toISOString();
    const latestDate = () => {
        if (feed.length === 0)
            return now();
        else
            return feed[feed.length - 1].date;
    };
    const soonestDate = () => {
        if (feed.length === 0)
            return now();
        else
            return feed[0].date;
    };

    function fetchOldMessages() {
        if (!logged) return;

        console.debug('Now please load new messages');
        setIsLoading(true);

        axios.get<FetchRoomMessagesResponse>(`/api/room/msg/get/${props.roomId}/10/before/${soonestDate()}`)
            .then(response => {
                if (response.data.success) {
                    setFeed(response.data.results.reverse().concat(feed));
                } else {
                    setNoMoreContent(true);
                }

                setIsLoading(false);
            })
            .catch(reason => {
                console.error(reason);
                alert('Attention : Une erreur est survenue.');
            });
    }

    useEffect(() => {
        if (!logged) return;

        // on change room
        setNoMoreContent(false);
        setIsLoading(true);
        axios.get<FetchRoomMessagesResponse>(`/api/room/msg/get/${props.roomId}/30/before/${now()}`)
            .then(response => {
                if (response.data.success) {
                    setFeed(response.data.results.reverse());
                    setNeedsScrollToBottom(true);
                    setIsLoading(false);
                }
            })
            .catch(error => {
                console.error(error);
                alert('Attention : une erreur est survenue.');
            });
    }, [logged, props.roomId]);

    useEffect(() => {
        if (root.current !== null && needsScrollToBottom) {
            root.current.scrollTop = root.current.scrollHeight + root.current.offsetHeight;
            setNeedsScrollToBottom(false);
        }
    }, [root, needsScrollToBottom]);

    useEffect(() => {
        if (root.current !== null && newMessagesCount > 0 && atBottom) {
            root.current.scroll(0, 2000);
            setNewMessagesCount(0);
        }
    }, [atBottom, root, newMessagesCount]);

    useEffect(() => {
        if (atBottom)
            setNewMessagesCount(0);
    }, [atBottom]);

    useInterval(2000, () => {
        if (!logged) return;

        axios.get<FetchRoomMessagesResponse>(`/api/room/msg/get/${props.roomId}/10/after/${latestDate()}`)
            .then(response => {
                if (response.data.success) {
                    setFeed(feed.concat(response.data.results.reverse()));
                    setNewMessagesCount(previousCount => {
                        return previousCount +
                            response.data.results.filter((e) => e.user.name !== user?.name).length
                    });
                }
                // else, no new messages available
            })
            .catch(reason => {
                console.error(reason);
                alert('Attention : Une erreur est survenue.')
            });
    });

    return (
        <div className="position-relative">
            <Col ref={root} style={{height: props.height + 'px', overflowY: 'scroll'}}>
                <Row className="align-items-center">
                    <Button
                        disabled={noMoreContent || !logged}
                        onClick={fetchOldMessages}
                        variant="link">
                        Voir les messages précédents
                    </Button>
                    { isLoading &&
                        <Spinner variant="primary" size="sm"  animation="border"/>
                    }
                </Row>

                { noMoreContent &&
                    <hr />
                }

                <div>
                    { feed.map(e =>
                        <RoomMessage key={e.id} message={e} />
                    ) }
                </div>
            </Col>

            { newMessagesCount > 0 &&
                <Button
                    variant="danger"
                    className="badge position-absolute"
                    style={{bottom: '6px'}}
                    onClick={() => setNeedsScrollToBottom(true)}>
                    {newMessagesCount} nouveau(x) message(s)
                </Button>
            }
        </div>
    );
}

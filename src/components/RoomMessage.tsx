import {useContext} from "react";
import { RoomMessageModel } from "meseji-types";
import {LogInContext} from "../utils/contexts";

interface Props {
    message: RoomMessageModel,
}

export default function RoomMessage(props: Props) {
    const {user} = useContext(LogInContext);
    const authorIsCurrentUser = user?.name === props.message.user.name;

    // TODO this code is difficult to read

    return (
        <div className={'mb-2 d-flex flex-column ' + (authorIsCurrentUser ? 'align-items-end' : 'align-items-start')}>
            <div className="small">
                <span className="font-weight-bolder">{props.message.user.name} </span>
                <span className="text-secondary">à {new Date(props.message.date).toLocaleTimeString()}</span>
            </div>
            <div className={'p-2 rounded ' + (authorIsCurrentUser ? 'bg-primary text-light' : 'bg-light')}>
                {props.message.content}
            </div>
        </div>
    );
}
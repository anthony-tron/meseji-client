import React, {useState} from "react";
import {Link} from "react-router-dom";
import axios, {AxiosResponse} from "axios";
import {Alert, Button, Form} from "react-bootstrap";
import {RegisterRequest, RegisterResponse} from "meseji-types";
import {useFormInput, useFormSubmit} from "../../utils/hooks";

export default function RegisterForm() {

    const [modal, setModal] = useState(<></>);

    const username = useFormInput('');
    const password = useFormInput('');

    const submit = useFormSubmit(() => {
        axios
            .post<RegisterRequest, AxiosResponse<RegisterResponse>>('/api/register', {
                username: username.value,
                password: password.value,
            })
            .then(response => {
                if (response.data.success) {
                    setModal(
                        <p>
                            Votre compte a été bien été créé.
                            <Link to="/login">Connectez-vous ici.</Link>
                        </p>
                    );
                } else {
                    setModal(
                        <Alert variant="warning">
                            Nous n'avons pas pu créé votre compte.
                            Veuillez vérifier que vous avez correctement rempli les champs.
                        </Alert>
                    );
                }
            })
            .catch(reason => {
                setModal(
                    <Alert variant="danger">
                        Désolé, une erreur est survenue.
                        Veuillez réessayer dans quelques instants.
                    </Alert>
                );
                console.error(reason);
            });
    });


    return (
        <div>
            {modal}
            <Form onSubmit={submit}>
                <Form.Group controlId="username">
                    <Form.Label>Identifiant</Form.Label>
                    <Form.Control {...username} name="username" type="text" placeholder="toto" />
                    <Form.Text>Le pseudo sous lequel vous apparaîtrez pour tout le monde.</Form.Text>
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Mot de passe</Form.Label>
                    <Form.Control {...password} name="password" type="password" placeholder="mot de passe" />
                    <Form.Text>Au moins 8 caractères</Form.Text>
                </Form.Group>

                <Button type="submit">Inscription</Button>
            </Form>
        </div>
    );
}

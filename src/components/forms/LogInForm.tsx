import {useFormInput, useFormSubmit} from "../../utils/hooks";
import {useState} from "react";
import axios, {AxiosResponse} from "axios";
import {LogInRequest, LogInResponse} from "meseji-types";
import {Alert, Button, Form} from "react-bootstrap";

export default function LogInForm() {
    const [modal, setModal] = useState(<></>);

    const username = useFormInput('');
    const password = useFormInput('');
    const submit = useFormSubmit(() => {
        axios
            .post<LogInRequest, AxiosResponse<LogInResponse>>('/api/login', {
                username: username.value,
                password: password.value,
            })
            .then((response) => {
                if (response.data.success) {
                    window.location.href = '/';
                } else {
                    setModal(
                        <Alert variant="warning">
                            Authentification invalide.
                            Veuillez vérifier que vos identifiants sont valides.
                        </Alert>
                    );
                }
            })
            .catch(reason => {
                setModal(
                    <Alert variant="danger">
                        Désolé, une erreur est survenue.
                        Veuillez réessayer dans quelques instants.
                    </Alert>
                );
                console.error(reason);
            });
    });

    return (
        <div>
            {modal}
            <Form onSubmit={submit}>
                <Form.Group controlId="username">
                    <Form.Label>Identifiant</Form.Label>
                    <Form.Control {...username} name="username" type="text" placeholder="toto" />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Mot de passe</Form.Label>
                    <Form.Control {...password} name="password" type="password" placeholder="mot de passe" />
                </Form.Group>

                <Button type="submit">Se connecter</Button>
            </Form>
        </div>
    );
}
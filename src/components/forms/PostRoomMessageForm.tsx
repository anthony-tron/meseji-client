import {useContext, useState} from "react";
import axios, {AxiosResponse} from "axios";
import {Form} from "react-bootstrap";
import {PostRoomMessageReponse, PostRoomMessageRequest} from "meseji-types";
import {LogInContext} from "../../utils/contexts";
import {useFormSubmit} from "../../utils/hooks";

interface Props {
    roomId: number,
}

export default function PostRoomMessageForm(props: Props) {

    const {logged} = useContext(LogInContext);

    const [content, setContent] = useState('');

    const submit = useFormSubmit(() => {
        if (content.length === 0)
            return;

        axios.post<PostRoomMessageRequest, AxiosResponse<PostRoomMessageReponse>>('/api/room/msg/post', {
            roomId: props.roomId,
            content: content,
        }).then((response => {
            if (!response.data.success)
                alert("message pas posté")
            else
                setContent('');
        })).catch(error => {
            alert(error);
        });
    });

    return (
        <Form onSubmit={submit}>
            <Form.Group>
                <Form.Control
                    disabled={!logged}
                    value={content}
                    onChange={(e) => setContent(e.target.value)}
                    name="content"
                    type="text"
                    placeholder="Écrivez un message..." />
            </Form.Group>
        </Form>
    );
}
import {useContext, useEffect, useState} from "react";
import {Link} from "react-router-dom";
import axios from "axios";
import {Nav, Navbar, Row, Spinner} from "react-bootstrap";
import {BrowseRoomsResponse, RoomModel} from "meseji-types";
import {LogInContext} from "../utils/contexts";
import RoomItem from "./RoomItem";


export default function RoomBrowser() {

    const [rooms, setRooms] = useState<RoomModel[]>([]);
    const {logged} = useContext(LogInContext);

    useEffect(() => {
        // once
        if (!logged) return;

        axios
            .get<BrowseRoomsResponse>('/api/room/browse')
            .then(response => {
                if (response.data.success)
                    setRooms(response.data.results);
                else
                    alert("Aucun salon n'a été trouvé, désolé !");
            })
            .catch(reason => {
                console.error(reason);
                alert('Attention : Un problème technique est survenu.');
            });
    }, [logged]);

    return (
        <Navbar className="card" collapseOnSelect expand="sm" sticky="top">
            <Row className="justify-content-between w-100">
                <Navbar.Brand>Salons de chat</Navbar.Brand>
                <Navbar.Toggle aria-controls="rooms-navbar"/>
            </Row>

            <Navbar.Collapse className="w-100" id="rooms-navbar">
                <Nav className="flex-column w-100">
                    { rooms.length > 0 &&
                        rooms.map(room =>
                            <Nav.Item className="w-100" key={room.id}>
                                <Nav.Link eventKey={room.id} as={Link} to={`/room/${room.urlName}`}>
                                    <RoomItem room={room} />
                                </Nav.Link>
                            </Nav.Item>
                        )
                    }
                    { rooms.length === 0 &&
                        <Spinner className="m-3" animation="border" role="status" variant="primary" size="sm">
                            <span className="sr-only">Loading...</span>
                        </Spinner>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>

    );
}

import {RoomModel} from "meseji-types";

interface Props {
    room: RoomModel,
}

export default function RoomItem(props: Props) {

    return (
        <div>
            <header>{props.room.name}</header>
        </div>
    );
}